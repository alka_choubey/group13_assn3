<?php include_once('../functions/functions.php');
    session_start();
    removeRecord();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/styles.css" type="text/css">
        <title>Pandora Network</title>
    </head>
    <body class="backing">
        <!-- Content beings here -->        
        <?php 
        if( $_SESSION['login'] == TRUE )
        {
        ?>
        <div class="container">
            <div class="row">
                <header class="page-header">
                    <h1>Administrator Data Entry</h1>
                </header>
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-header extraPadding">
                            <h2>Are you sure you wish to remove this item?</h2>
                        </div>
                        <div class="panel-body customPanel">
                            
                            <form method='POST' >
                                <input type="hidden" name="id" value="<?php echo displayID(); ?>">
                                <input class="btn btn-danger" type="submit" name="removeItem" value="Delete Item">
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
