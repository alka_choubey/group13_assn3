<?php 
include_once('../functions/functions.php');
    try
    {
        $logoutdata = $_POST["logoutclicked"];
        $navigateeditid = (int)$_POST["edit"];
        $navigate = $_POST["addnew"];
        if(!isset($_SESSION['loginkey'])){
            session_start();
             $username = $_POST["username"];
             $password = $_POST["password"]; 
           if(isset($username) && isset($password)){
                if(login_admin($username,$password))
                {                   
                    $_SESSION["loginkey"] = $username;
                }
                else
                {                   
                    $_SESSION["loginkey"] = null;
                    session_destroy();
                }                  

           }                                
        }
        if($logoutdata == 'Logout'){            
            logout();
        } 

        if($navigateeditid > 0){
            header('Location: ../admin/edit.php?id='. $navigateeditid);
        }  

        if($navigate == 'Add new Panel'){
            header('Location: ../admin/add.php');
        }  
        $delete = (int)$_POST["delete"];

        if($delete > 0){
            delete_item($delete);
        }  
    }
    catch(Exception $e)
    {
        echo $e->getMessage();
    }
 ?>

<!doctype html>
<html>
  <head>
    <meta charset="utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>

          <!-- Optional theme -->
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"/>

            <link rel="stylesheet" href="../css/bootstrap-select.min.css" type="text/css" />
              <link rel="stylesheet" href="../css/styles.css" type="text/css"/>
                <title>Pandora Network</title>
              </head>
  <body>
    <!-- Content begins here -->
    <div id="header"></div>
    <?php 
    if(isset($_SESSION['loginkey']))
    {        
    ?>
    <div class="container">
      <div class="row">
        <header class="page-header">
          <h1>Content Management Main Menu</h1>
        </header>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <h2>
            Welcome <?php echo  $username;  ?>
          </h2>
        </div>
        <div class="col-sm-3" style="padding-top:20px">
          <select class="selectpicker" id="pagetype" name="page"
              title="Choose one of the following..." onchange="Pagechanged('admin')">
            <option>Home</option>
            <option>Links</option>
            <option>Info</option>
            <option>FAQs</option>
          </select>
        </div>
        <form method='POST' style="padding-top:20px">
          <input class="btn btn-default" type="submit" name="addnew" value="Add new Panel"/>
            &nbsp;&nbsp;&nbsp;
            <input class="btn btn-default" type="submit" name="logoutclicked" value="Logout"/>                           
                </form>
      </div>
      <div class="row">
        <div id="dvLoading">
          <p>
            <img src="../images/ajax-loader.gif" />
          </p>
        </div>
        <div class="panel panel-default">
          <form method='POST'>
            <table id="tblpage" class="table table-condensed">

             </table>
          </form>
        </div>
      </div>
    </div>
    <?php  
    }
        else
        {
            header('Location: ../admin/login.php');
        }
    ?>

    <div id="footer"></div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type="text/javascript">
      var mainPages = false;
    </script>
    <script src="../js/script.js">
    </script>
    <script src="../js/bootstrap-select.min.js"></script>
  </body>
</html>