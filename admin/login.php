<?php 
//include_once('../functions/functions.php');
    try
    {
        // if(!isset($_SESSION['loginkey'])){
        //     session_start();
            $username = $_POST["username"];
            $password = $_POST["password"];
        //     if(login_admin($username,$password))
        //     {
        //         $_SESSION["loginkey"] = $username;
        //     }
        // }
    }
    catch(Exception $e)
    {
        echo $e->getMessage();
    }
 ?>

<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

          <!-- Optional theme -->
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

            <!-- Custom CSS -->
            <link rel="stylesheet" href="../css/styles.css" type="text/css">
              <title>Pandora Network</title>
            </head>
  <body>
    <!-- Content begins here -->
    <div id="header"></div>
    <div class="container">
      <div class="row">
        <header class="page-header">
          <h1>Admin Page Login</h1>
        </header>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-header extraPadding">
              <h2>Please enter in your credentials to login:</h2>
            </div>
            <div class="panel-body">
              <form method="POST" action="index.php">
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1">Username</span>
                  <input type="text" class="form-control" name="username" placeholder="Username" aria-describedby="basic-addon1">
                            </div>
                <br>
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Password</span>
                    <input type="password" class="form-control" name="password" placeholder="Password" aria-describedby="basic-addon1">
                            </div>
                  <br>
                    <button type="submit" name="login" class="btn btn-success">Submit</button>
                  </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="footer"></div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type="text/javascript">
      var mainPages = false;
    </script>
    <script src="../js/script.js"></script>
  </body>
</html>