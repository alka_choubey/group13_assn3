<?php include_once('../functions/functions.php');
session_start();
$id = $_REQUEST['id'];

    if(isset($_SESSION['loginkey'])){
//Id, header, description, pagename, headerorder
        $row_record = get_record_by_id($id);
        $panelheader = $row_record[1];
        $description = $row_record[2];
        $pagename = $row_record[3];
        $headerorder = $row_record[4];
        $externallink = $row_record[5];
        $imgsrc = $row_record[6];
        $updateitem = $_POST["updateitem"];
        //  
        if($updateitem == "Edit"){

            $newpanelheader = $_POST["panelheader"];     
     
            $newdescription = $_POST["description"];         
             $newpagename = $_POST["pagename"];          
             $newheaderorder = $_POST["headerorder"];  
             $newexternallink = $_POST["externallink"];   
             $newimgsrc = $_POST["imgsrc"];           
             $success = true;
     
            if($panelheader != $newpanelheader || $description != $newdescription || $pagename != $newpagename
            || $headerorder != $newheaderorder || $externallink != $newexternallink || $newimgsrc != $imgsrc){
               
                $success = update_item_by_id($id, $newpanelheader, $newdescription, 
                $newpagename, $newheaderorder, $newexternallink, $newimgsrc);
            }  
            if($success){
                header('Location: ../admin/index.php');
            }
         }
    }

  ?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Edit Item</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

          <!-- Optional theme -->
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

            <!-- Custom CSS -->
            <link rel="stylesheet" href="../css/bootstrap-select.min.css" type="text/css" >
              <link rel="stylesheet" href="../css/styles.css" type="text/css">
                <script type="text/javascript">
                  var oldpanelheader = "";
                  var olddescription = "";
                  var oldpagename = "";
                  var oldheaderorder = "";
                  var oldexternallink = "";
                  var oldimgsrc = "";
                  window.onload=function(){
                  oldpanelheader = document.getElementById("panelheader").value;
                  olddescription  = document.getElementById("description").value;
                  oldpagename  = document.getElementById("pagename").value;
                  oldheaderorder  = document.getElementById("headerorder").value;
                  oldexternallink  = document.getElementById("externallink").value;
                  oldimgsrc  = document.getElementById("imgsrc").value;
                  };
                  //keeping the old value on load
                  //and then checking if the values have changed on
                  //submit. so that unnecessary update and database call
                  //is saved.
                </script>
              </head>
  <body>
    <div id="header"></div>
    <?php 
        if( $_SESSION['loginkey'] == TRUE )
        {
        ?>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-danger">
            <div class="panel-header extraPadding">
              <h2>Edit Panel</h2>
            </div>
            <div class="panel-body customPanel">

              <form method="POST">
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1">Panel Header</span>
                  <input type="text" class="form-control" id="panelheader" name="panelheader" placeholder="Name of Header"
                   aria-describedby="basic-addon1" value="<?php echo $panelheader; ?>">
                </div>
                <br>
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Panel Header Order</span>
                    <input class="form-control" id="headerorder" name="headerorder" placeholder="1" type="number" step="1"
                    aria-describedby="basic-addon1" value="<?php echo $headerorder; ?>">
                  </div>
                  <br>
                    <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">Panel Text</span>
                      <textarea  class="form-control" id="description" name="description"
                      placeholder="Information" aria-describedby="basic-addon1" style="height:150px;"
                      value="<?php echo $description; ?>"><?php echo $description; ?>
                      </textarea>
                    </div>
                    <br>
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">External Link</span>
                        <input type="text" class="form-control" id="externallink" name="externallink"
                        placeholder="External Link" value="<?php echo $externallink; ?>" 
                        aria-describedby="basic-addon1">
                      </div>
                      <br/>
                        <div class="input-group">
                          <span class="input-group-addon" id="basic-addon1">Image Source</span>
                          <input type="text" class="form-control" id="imgsrc" name="imgsrc"
                          placeholder="Image Source" value="<?php echo $imgsrc; ?>"
                          aria-describedby="basic-addon1"/>
                        </div>
                        <br/>
                          <?php echo get_selected_item($pagename); ?>
                          <br/>
                            <br/>
                              <button type="submit" name="updateitem" class="btn btn-success" onClick="return validate()"
                              value="Edit">Submit</button>
                            </form>

            </div>
          </div>
        </div>
      </div>
    </div>
    <?php  
        }
        else
        {
            header('Location: ../admin/login.php');      
        }
        ?>
    <div id="footer"></div>
    <!-- Latest compiled and minified JavaScript -->
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type="text/javascript">
      var mainPages = false;
    </script>
    <script src="../js/script.js">
    </script>
    <script src="../js/bootstrap-select.min.js"></script>
  </body>
</html>