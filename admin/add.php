<?php include_once('../functions/functions.php');
    session_start();
 ?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Add Panel</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

          <!-- Optional theme -->
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

            <!-- Custom CSS -->
            <link rel="stylesheet" href="../css/bootstrap-select.min.css" type="text/css" >
              <link rel="stylesheet" href="../css/styles.css" type="text/css">

    </head>
  <body>
    <div id="header"></div>
    <?php 
        if( $_SESSION['loginkey'] == TRUE )
        {
        ?>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-danger">
            <div class="panel-header extraPadding">
              <h2>Add Panel</h2>
            </div>
            <div class="panel-body customPanel">

              <form method="POST">
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1">Panel Header</span>
                  <input type="text" class="form-control" id="panelheader" name="panelheader" placeholder="Name of Header"
                  aria-describedby="basic-addon1">
                  </div>
                <br>
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Panel Header Order</span>
                    <input class="form-control" id="headerorder" name="headerorder" placeholder="1" type="number" step="1"
                    aria-describedby="basic-addon1">
                    </div>
                  <br>
                    <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">Panel Text</span>
                      <textarea  class="form-control" id="description" name="description"
                      placeholder="Information" aria-describedby="basic-addon1" style="height:150px;"></textarea>
                    </div>
                    <br>
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">External Link</span>
                        <input type="text" class="form-control" id="externallink" name="externallink"
                        placeholder="External Link"
                        aria-describedby="basic-addon1">
                        </div>
                      <br>
                        <div class="input-group">
                          <span class="input-group-addon" id="basic-addon1">Image Source</span>
                          <input type="text" class="form-control" id="imgsrc" name="imgsrc"
                          placeholder="Image Source"
                          aria-describedby="basic-addon1">
                          </div>
                        <br>
                          <select class="selectpicker" id="pagename" name="pagename"
                              title="Choose one of the following...">
                            <option>Home</option>
                            <option>Links</option>
                            <option>Info</option>
                            <option>FAQs</option>
                          </select>
                          <br>
                            <br>
                              <button type="submit" name="addItem" class="btn btn-success" onClick="return empty()"
                              value="Add">Submit</button>
                            </form>

              <h2>
                <a href="./index.php">
                  <button class="btn btn-info" style="width:200px">Go back to admin screen</button>
                </a>
              </h2>
              <h3>
                <?php
                                    if(isset($_SESSION['loginkey'])){
                                        $newpanelheader = $_POST["panelheader"];     
                                        $newdescription = $_POST["description"];         
                                        $newpagename = $_POST["pagename"];          
                                        $newheaderorder = $_POST["headerorder"]; 
                                        $externallink = $_POST["externallink"]; 
                                        $imgsrc = $_POST["imgsrc"]; 
                                        if(isset($newpanelheader) && (isset($newdescription) || isset($externallink)) && isset($newpagename)){                                            
                                            add_record($newpanelheader, $newdescription, $newpagename, 
                                            $newheaderorder, $externallink, $imgsrc); 
                                        } 
                                        $_POST["panelheader"] = "";
                                        $_POST["description"] = "";
                                        $_POST["pagename"] = "";       
                                        $_POST["externallink"] = "";     
                                        $_POST["imgsrc"] = "";                                                                                                        
                                    }
                            ?>
              </h3>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php  
        }
        else
        {
            header('Location: ../admin/login.php');      
        }
        ?>
    <div id="footer"></div>
    <!-- Latest compiled and minified JavaScript -->
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type="text/javascript">
      var mainPages = false;
    </script>
    <script src="../js/script.js">
    </script>
    <script src="../js/bootstrap-select.min.js"></script>
  </body>
</html>