SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `comp6002_g13_assn3` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `comp6002_g13_assn3`;

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
    `ADMIN_ID` int(11) NOT NULL AUTO_INCREMENT,
    `NAME` varchar(20) NOT NULL,
    `PASSWRD` varchar(20) NOT NULL,
    `CATEGORY` varchar(11) NOT NULL,
    PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_admin` (`ID`, `NAME`, `PASSWRD`, `CATEGORY`) VALUES
(1, 'Jeff', 'jeff', '');


DROP TABLE IF EXISTS `tbl_images`;
CREATE TABLE `tbl_images` (
    `IMAGE_ID` INT NOT NULL AUTO_INCREMENT,
    `IMAGE_NAME` varchar(30),
    `SOURCE` varchar(90),
    PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `tbl_pages` (
  `Id` int(11) NOT NULL,
  `header` varchar(200) NOT NULL AUTO_INCREMENT,
  `description` text NULL,
  `pagename` varchar(100) NOT NULL,
  `headerorder` int(11) NOT NULL,
  `externallink` varchar(500) NULL 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tbl_pages`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `idx_primary` (`Id`);

  ALTER TABLE `tbl_pages` ADD `imgsrc` VARCHAR(100) NULL AFTER `externallink`;
