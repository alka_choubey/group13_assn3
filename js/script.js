$(function(){

  if(mainPages){
      $('#dvLoading').show();
  $("#header").append('<nav class="navbar navbar-default "><div class="container-fluid">'
    + '<div class="navbar-header mynavbar"><button type="button" class="navbar-toggle collapsed " '
    + 'data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">'
      + '<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span>'
      + '<span class="icon-bar"></span><span class="icon-bar"></span></button></div>'
    + '<div class="collapse navbar-collapse mynavbar" id="bs-example-navbar-collapse-1">' 
     + '<ul class="nav navbar-nav mynavbar"><li class="active"><a href="index.html">Home '
      + '<span class="sr-only">(current)</span></a></li>' 
      +  '<li><a href="links.html">Links</a></li>'
       + '<li><a href="info.html">Info</a></li>'
       + '<li><a href="faqs.html">FAQs</a></li>'
       + '<li><a href="https://www.boppoly.ac.nz/go/elearning">Moodle</a></li>'				 
       + '<li class="dropdown">'
        + '<a href="index.html" class="dropdown-toggle" data-toggle="dropdown" role="button" '
        + 'aria-haspopup="true" aria-expanded="false">Dropdown<span class="caret"></span></a>'
         + '<ul class="dropdown-menu"><li><a href="index.html">Home</a></li>'
          +  '<li><a href="links.html">Links</a></li>'
          + '<li><a href="info.html">Info</a></li>'
           + '<li><a href="faqs.html">FAQs</a></li><li role="separator" class="divider"></li>'
           + '<li><a href="https://www.boppoly.ac.nz/go/elearning">Moodle</a></li>'
          + '</ul></li></ul>'
		+ '<ul class="nav navbar-nav navbar-right"><li><a href="admin/login.php">Admin</a></li></ul>'				
    + '</div></div></nav>'); 
  $("#footer").append('<footer class="logos"><div class="container"><div class="row">'
			+ '<div class="col-sm-9"><div class="panel">'
				+'<img src="Images/toi-ohomai-logo.jpg" alt="Polytech logo"/></div>'
			+ '</div><div class="col-sm-3">'
        + '<div class="panel"><img src="Images/uowlogo.gif" alt="Waikato University logo"/>'
         + '</div></div></div></div></footer>'); 

    var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var jsonObj = $.parseJSON(this.responseText);
                $('#divrow').html(jsonObj.headerdata);  
                if(pagename == "faqs"){
                    $('#linkdivrow').html(jsonObj.linksdata);  
                }
                $('#dvLoading').hide();          
            }
         
        };
        xmlhttp.open("GET", "admin/ajax.php?pagename=" + pagename, true);
        xmlhttp.send();


}
else{
  $("#header").append('<nav class="navbar navbar-default "><div class="container-fluid">'
    + '<div class="navbar-header mynavbar"><button type="button" class="navbar-toggle collapsed " '
    + 'data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">'
      + '<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span>'
      + '<span class="icon-bar"></span><span class="icon-bar"></span></button></div>'
    + '<div class="collapse navbar-collapse mynavbar" id="bs-example-navbar-collapse-1">' 
     + '<ul class="nav navbar-nav mynavbar"><li class="active"><a href="../index.html">Home '
      + '<span class="sr-only">(current)</span></a></li>' 
      +  '<li><a href="../links.html">Links</a></li>'
       + '<li><a href="../info.html">Info</a></li>'
       + '<li><a href="../faqs.html">FAQs</a></li>'
       + '<li><a href="https://www.boppoly.ac.nz/go/elearning">Moodle</a></li>'				 
       + '<li class="dropdown">'
        + '<a href="index.html" class="dropdown-toggle" data-toggle="dropdown" role="button" '
        + 'aria-haspopup="true" aria-expanded="false">Dropdown<span class="caret"></span></a>'
         + '<ul class="dropdown-menu"><li><a href="index.html">Home</a></li>'
          +  '<li><a href="../links.html">Links</a></li>'
          + '<li><a href="../info.html">Info</a></li>'
           + '<li><a href="../faqs.html">FAQs</a></li><li role="separator" class="divider"></li>'
           + '<li><a href="https://www.boppoly.ac.nz/go/elearning">Moodle</a></li>'
          + '</ul></li></ul>'					
    + '</div></div></nav>'); 
  $("#footer").append('<footer class="logos"><div class="container"><div class="row">'
			+ '<div class="col-sm-9"><div class="panel">'
				+'<img src="../Images/toi-ohomai-logo.jpg" alt="Polytech logo"/></div>'
			+ '</div><div class="col-sm-3">'
        + '<div class="panel"><img src="../Images/uowlogo.gif" alt="Waikato University logo"/>'
         + '</div></div></div></div></footer>'); 

          $('#dvLoading').hide();

}
});


function Pagechanged(admin) {            
        $('#dvLoading').show();
    var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                $('#tblpage').html('<tr><th width=20px></th><th width=20px></th><th width=20px>'
                               + ' Order</th><th width=150px>Header</th><th>Text</th><th>External links</th></tr></thead>' + this.responseText);
                $('#dvLoading').hide();
            }
         
        };
        xmlhttp.open("GET", "ajax.php?pagename=" + $("#pagetype").val() + "&admin=" + admin, true);
        xmlhttp.send();
}

function empty()
{
    //checking for all the data is entered before 
    //the page is posted to the server so that an unnecesary trip is saved.
    var result = true;
    var msg = "";
    var panelheader;
    panelheader = document.getElementById("panelheader").value;
    var description  = document.getElementById("description").value;
    var pagename  = document.getElementById("pagename").value;
    var headerorder  = document.getElementById("headerorder").value;
    var externallink  = document.getElementById("externallink").value;
    if (panelheader == "") {
        msg = "Enter a Panel header";
        result = false;
    }
    else if (description == "" && externallink == ""){
              msg = "Enter a description or externallink";
            result = false;
    }
    else if(pagename == ""){
              msg = "Select a page";
            result = false;                    
    }
    if(msg != ""){
        alert(msg);
    }
    return result;          
}

function validate()
{
    var result = true;
    var msg = "";
    var panelheader;
    panelheader = document.getElementById("panelheader").value;
    var description  = document.getElementById("description").value;
    var pagename  = document.getElementById("pagename").value;
    var headerorder  = document.getElementById("headerorder").value;
    var externallink  = document.getElementById("externallink").value;
    var imgsrc  = document.getElementById("imgsrc").value;
    if (panelheader == "") {
        msg = "Enter a Panel header";
        result = false;
    }
    else if (description == "" && externallink == ""){
              msg = "Enter a description or externallink";
            result = false;
    }
    else if(pagename == ""){
              msg = "Select a page";
            result = false;                    
    }
    else 
    {
        if(oldpanelheader == panelheader && olddescription == description && pagename == oldpagename
        && headerorder == oldheaderorder && oldexternallink == externallink && oldimgsrc == imgsrc){
            result = false;
            window.history.back();
        } 
    }
    if(msg != ""){
        alert(msg);
    }
    return result;          
}