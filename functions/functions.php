<?php

 function opendb()
 {
     $servername  = 'localhost';
     $username = 'root';
     $password = 'root';
     $dbname = 'comp6002_G13_assn3';

     $conn = mysqli_connect($servername, $username, $password, $dbname);
     if ($conn->connect_errno > 0) {
         die('Unable to connect to database ['.$conn->connect_errno.']');
     }
// some change
     return $conn;
 }

 function login_admin($username, $password)
{

    $accessed = false;
    $query = "select * from comp6002_G13_assn3.tbl_admin where NAME='$username' and PASSWRD='$password'";
    $conn = opendb();

    $result = mysqli_query($conn, $query) or die ('error query failed accessing data');

    $num_rows = mysqli_fetch_row($result);
    if($num_rows)
    {
       $accessed = true;
    }
    return $accessed;
    mysql_close($conn);
}

function get_all_items($pagename)
{
    $row_record = array();
    try{
        $query = "select p.Id, p.headerorder, p.header, p.description, p.externallink, p.imgsrc  from  " .
            "comp6002_G13_assn3.tbl_pages p where p.pagename ='$pagename' order by p.headerorder";

        $conn = opendb();

        $result = mysqli_query($conn, $query) or die ('error query failed getting data');

        while ($num_rows = mysqli_fetch_row($result)) {
            $row_record[] = $num_rows;
        }   
    }
    catch(Exception $e){
         echo $e->getMessage();
    }    
    return $row_record;
    mysql_close($conn);
}

function show_all_items($row_record)
{
    for($i = 0; $i < sizeof($row_record); $i++)
    {
        print '<tr>';

        $rec = $row_record[$i];

        print '<td><button class="btn btn-warning" type="submit" name="delete" value="'.  $row_record[$i][0] . '">Delete</button>' .
            '</td><td><button type="submit" name="edit" value="'.  $row_record[$i][0] . '" class="btn btn-info">Edit</button></td>';
        for ($j = 1; $j < sizeof($rec); $j++) 
        { 
            print '<td>' .  $row_record[$i][$j] .  '</td>';              
        }
        print '</tr>';
    }
    print '<tr><td colspan=7>Total Records ' .  sizeof($row_record) . '</td></tr>';
}

function logout(){
   session_destroy(); 
     $_SESSION["loginkey"] = null;

}

function add_record($newpanelheader, $newdescription, $newpagename,
            $newheaderorder, $externallink, $imgsrc)
{
    try
    {
        if(isset($newpanelheader) && (isset($newdescription) || isset($externallink)) && isset($newpagename)){
            if($newpanelheader != '' && ($newdescription != '' || $externallink != '')  && $newpagename != ''){

                $headerordr = 0;
                if(isset($newheaderorder)){
                    if($newheaderorder == "")
                    {                   
                        $headerordr = 0;
                    }
                    else if(ctype_digit($newheaderorder) == true)
                    {
                        $headerordr =  (int)$newheaderorder;
                    }
                }
                if(!isset($externallink)){
                    $externallink = "";
                }
                if(!isset($newdescription)){
                    $newdescription = "";
                }

                if(!isset($imgsrc)){
                    $imgsrc = "";
                }
                $conn = opendb();

                $checkquery = "select * from  comp6002_G13_assn3.tbl_pages " .
                " where header ='$newpanelheader' AND description = '$newdescription'   AND pagename = '$newpagename'";

                $result = mysqli_query($conn, $checkquery) or die ('error query failed getting data');

                if(mysqli_fetch_row($result)){
                    echo $newpanelheader . " already added";
                }
                else{
                    $query = "INSERT INTO comp6002_G13_assn3.tbl_pages (header, description, pagename, headerorder, externallink, imgsrc)  " .
                    "VALUES('". $newpanelheader . "','" . $newdescription . "','" . $newpagename . "'," 
                    . $headerordr. ", '" . $externallink . "','" . $imgsrc . "')";
           
                    if(mysqli_query($conn, $query)){
                        echo "Records inserted successfully.";
                    } else{
                        echo "ERROR: Could not able to execute $query. " . mysqli_error($conn);
                    }
                }
            }
        }
    }
    catch(Exception $e){
         echo $e->getMessage();
    }
    mysql_close($conn);
}

function delete_item($id)
{
    try
    {
        if(isset($id) && $id > 0){
             $conn = opendb();

            $deletequery = "delete from  comp6002_G13_assn3.tbl_pages " .
            " where id =" . $id;
            
            $result = mysqli_query($conn, $deletequery) or die ('error query failed deleting data');
        }
    }
    catch(Exception $e){
         echo $e->getMessage();
    }
    
}

function get_record_by_id($id)
{
    $row_record = array();
    try
    {
        if(isset($id) && $id > 0){
            $conn = opendb();

            $getquery = "select Id, header, description, pagename, headerorder, externallink, imgsrc from  comp6002_G13_assn3.tbl_pages " .
            " where id =" . $id;
            
            $result = mysqli_query($conn, $getquery) or die ('error query failed getting data');

            while ($num_rows = mysqli_fetch_row($result)) {
               $row_record = $num_rows;
            }
        }
    }
    catch(Exception $e){
         echo $e->getMessage();
    }
    return $row_record;
    mysql_close($conn);
}

function get_selected_item($pagename)
{
    $option = "";

    switch (strtolower($pagename)) {
    case "home":
        $option = "<option selected>Home</option><option>Links</option><option>Info</option><option>FAQs</option>";
        break;
    case "links":
        $option = "<option>Home</option><option selected>Links</option><option>Info</option><option>FAQs</option>";
        break;
    case "info":
        $option = "<option>Home</option><option>Links</option><option selected>Info</option><option>FAQs</option>";
        break;
    case "faqs":
       $option = "<option>Home</option><option>Links</option><option>Info</option><option selected>FAQs</option>";
        break;
    }
    print '<select class="selectpicker" id="pagename" name="pagename">' . $option .
            '</select>';

}

function update_item_by_id($id, $newheader, $newdescription, $newpagename, $newheaderorder, $newexternallink, $imgsrc)
{
    $success = false;
    try
    {
        if(isset($id) && $id > 0 && isset($newheader)  && isset($newpagename)        
        && $newheader != ''  && $newpagename != ''){
           
            $headerordr = 0;
            if(isset($newheaderorder)){
                if($newheaderorder == "")
                {                   
                    $headerordr = 0;
                }
                else if(ctype_digit($newheaderorder) == true)
                {
                    $headerordr =  (int)$newheaderorder;
                }
            }
            if(!isset($newexternallink)){
                $newexternallink = "";
            }
            if(!isset($imgsrc)){
                $imgsrc = "";
            }
            if(!isset($newdescription)){
                $newdescription = "";
            }
            $conn = opendb();

            $updatequery = "update comp6002_G13_assn3.tbl_pages set header = '" . $newheader . "', description = '"
             . $newdescription .
                      "', pagename = '" . $newpagename . "', headerorder = " . $headerordr .  
                      ", externallink = '" . $newexternallink . "', imgsrc = '". $imgsrc . "' where id = " . $id;
                      
            $result = mysqli_query($conn, $updatequery) or die ('error query failed updating data');

            if($result)
            {
                $success = true;
            }
        }
    }
    catch(Exception $e){
         echo $e->getMessage();
    }
    
    return $success;
    mysql_close($conn);
}


function show_all_items_non_admin($row_record, $pagename)
{
    $headerdata = "";
    $linksdata = "";
    for($i = 0; $i < sizeof($row_record); $i++)
    {
        if($pagename != "links"){
            if(isset( $row_record[$i][3]) &&  $row_record[$i][3] != ""){
                $headerdata = $headerdata . '<div class="col-sm-4"><div class="panel panel-default"><div class="panel-heading">'
                . '<h3 class="panel-title">' . $row_record[$i][2] . '</h3></div><div class="panel-body"><ul>';
                $lipoints  = explode('<br>', $row_record[$i][3]);

                for($j = 0; $j < sizeof($lipoints); $j++){
                    $headerdata = $headerdata . '<li class="info">' . $lipoints[$j] . '</li>';
                }
                $headerdata = $headerdata . '</ul></div></div></div>';
            }
            if(isset( $row_record[$i][4]) &&  $row_record[$i][4] != ""){
                if($pagename == "faqs"){
                    $linksdata = $linksdata . '<div class="col-sm-6"><div class="panel">'
                    . '<iframe width="450" height="300" src="' 
                    . $row_record[$i][4] . '" frameborder="0" allowfullscreen></iframe></div></div>';
                }
            }
        }
        else
        {
            $headerdata = $headerdata . '<div class="col-sm-4"><div class="panel"><a href="' . $row_record[$i][4]. '" target="_blank">'
            . '<img class="block-img" src="' . $row_record[$i][5] . '" alt="Moodle"/> </a><figcaption>'
            . $row_record[$i][2] . '</figcaption></div></div>';  
        }
    }
    $data = array("headerdata"=>$headerdata, "linksdata"=>$linksdata);
    return $data;    
}
?>